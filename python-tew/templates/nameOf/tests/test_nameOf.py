# -*- coding: utf-8 -*-
from <%= snakeName %>.<%= snakeName %> import <%= snakeName %>


class <%= CamelName %>TestCase(unittest.TestCase):
    def test_<%= snakeName %>(self):
        self.assertEqual(<%= snakeName %>(), "<%= camelName %>")
