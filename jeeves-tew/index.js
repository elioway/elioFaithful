"use strict"
const Thingrator = require("../thingrator")

module.exports = class Jeevator extends Thingrator {
  constructor(args, opts) {
    super(args, opts)
    this.argument("elioName", { type: String, required: false })
    this.argument("elioGroup", { type: String, required: false })
    this.argument("cheat", { type: String, required: false })
  }
  writing() {
    this.paths()
    let cheatSubject = this.options.cheat
    if (cheatSubject) {
      this._write("doc/cheat.subjectOf.md", `doc/cheat.${cheatSubject}.md`, {
        cheatSubject: cheatSubject,
      })
    }
  }
  end() {
    if (this.options.cheat) this._eliosay("cheat!")
  }
}
