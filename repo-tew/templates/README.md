![](https://elioway.gitlab.io/<%= elioGroup %>/<%= elioName %>/elio-<%= kebabName %>-logo.png)

> <%= CamelName %>, **the elioWay**

# <%= elioName %>

Starter pack for an elioThing app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [<%= elioName %> Documentation](https://elioway.gitlab.io/<%= elioGroup %>/<%= elioName %>/)

## Installing

- [Installing <%= elioName %>](https://elioway.gitlab.io/<%= elioGroup %>/<%= elioName %>/installing.html)

## Requirements

- [<%= elioGroup %> Prerequisites](https://elioway.gitlab.io/<%= elioGroup %>/installing.html)

## Getting To Know Yeoman

- Yeoman has a heart of gold.
- Yeoman is a person with feelings and opinions, but is very easy to work with.
- Yeoman can be too opinionated at times but is easily convinced not to be.
- Feel free to [learn more about Yeoman](http://yeoman.io/).

## Seeing is Believing

```
You're seeing it.
```

- [<%= elioGroup %> Quickstart](https://elioway.gitlab.io/<%= elioGroup %>/quickstart.html)
- [<%= elioName %> Quickstart](https://elioway.gitlab.io/<%= elioGroup %>/<%= elioName %>/quickstart.html)

# Credits

- [<%= elioName %> Credits](https://elioway.gitlab.io/<%= elioGroup %>/<%= elioName %>/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/<%= elioGroup %>/<%= elioName %>/apple-touch-icon.png)
