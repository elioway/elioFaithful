# Installing <%= elioName %>

## Prerequisites

- [prerequisites](./prerequisites)

## Development

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/<%= elioGroup %>.git
cd <%= elioGroup %>
git clone https://gitlab.com/<%= elioGroup %>/<%= elioName %>.git
```
