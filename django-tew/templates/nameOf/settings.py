# -*- encoding: utf-8 -*-
import os
from dna.settings import *

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "SECRET_KEY"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "easy_thumbnails",
    "dna",
    "<%= snakeName %>",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "<%= snakeName %>.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

WSGI_APPLICATION = "<%= snakeName %>.wsgi.application"

DATABASES = {
    "default": {"ENGINE": "django.db.backends.sqlite3", "NAME": "<%= snakeName %>.db"}
}

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"
    },
]

LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True

APP_NAME = "<%= snakeName %>"

STATIC_ROOT = "static"

STATIC_URL = "/<%= snakeName %>_static/"

MEDIA_ROOT = "media"

MEDIA_URL = "/<%= snakeName %>_media/"

DNA_DEPTH = 0
SITE_DNA = {
    "page": {
        "Home": {
            "engage": {"WebPage": {"field": ["name", "description"]}},
            "list": {"Photograph": {"field": ["name", "image"]}},
        },
        "Blog": {
            "engage": {"WebPage": {
                "field": ["name", "description"],
                "many": ["image"]
                }
            },
            "list": {
                "WebPage": {"field": ["name", "description", "image"]}
            },
        },
        "About": {
            "engage": {
                "AboutPage": {"field": ["name", "description"]},
                "Organization": {
                    "field": ["name", "address", "telephone", "email"],
                    "many": ["contactPoints", "reviews"]
                },
            }
        },
        "Logout": {"url": "/"},
    }
}

SCHEMA_VERSION = "3.9"
SCHEMA_PATH = f"schemaorg/data/releases/{SCHEMA_VERSION}/all-layers.jsonld"
