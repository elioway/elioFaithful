![](https://elioway.gitlab.io/<%= elioGroup %>/<%= elioName %>/elio-<%= kebabName %>-logo.png)

> <%= CamelName %>, **the elioWay**

# <%= elioName %>

Your new creature built **the elioWay**.

- [dna Documentation](https://elioway.gitlab.io/eliothing/dna)

## Seeing is believing

```shell
virtualenv --python=python3 venv-<%= snakeName %>
source venv-<%= snakeName %>/bin/activate.fish
pip install -r requirements/local.txt
./run_<%= snakeName %>.sh
```

## genome Management command

To help you find the right schema Thing, use our genome explorer: `django-admin genome <class/property>`

```shell
django-admin genome Thing
django-admin genome Person
django-admin genome isbn
django-admin genome description
django-admin genome image
```

## Nutshell

```
django-admin runserver 0.0.0.0:8000
```

- [<%= elioGroup %> Quickstart](https://elioway.gitlab.io/<%= elioGroup %>/quickstart.html)
- [<%= elioName %> Quickstart](https://elioway.gitlab.io/<%= elioGroup %>/<%= elioName %>/quickstart.html)

# Credits

- [<%= elioName %> Credits](https://elioway.gitlab.io/<%= elioGroup %>/<%= elioName %>/credits.html)

## License

[MIT](LICENSE) [Tim Bushell](mailto:tcbushell@gmail.com)

![](https://elioway.gitlab.io/<%= elioGroup %>/<%= elioName %>/apple-touch-icon.png)
