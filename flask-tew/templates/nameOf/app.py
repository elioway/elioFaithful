import flask
import os
from <%= snakeName %>.jinja_filters import qs_active, qs_toggler
from <%= snakeName %>.views import Main, <%= CamelName %>

config_objects = {
    "development": "<%= snakeName %>.config.DevelopmentConfig",
    "testing": "<%= snakeName %>.config.TestingConfig",
    "default": "<%= snakeName %>.config.DevelopmentConfig",
}

# You just use two optional env variables.
CONFIG_TYPE = os.getenv("CONFIG_TYPE", "default")
CONFIG_PATH = os.getenv("CONFIG_PATH", "./<%= snakeName %>/<%= snakeName %>.cfg")

default_config_path = os.path.abspath(CONFIG_PATH)
print("default_config_path", default_config_path)

# Doing it the application factory way.
def create_app(config_path=default_config_path):

    app = flask.Flask(__name__)
    # Get the default settings (with the demo MP3 collection).
    app.config.from_object(config_objects[CONFIG_TYPE])
    # Get the custom settings.
    app.config.from_pyfile(config_path)

    SECRET_KEY = app.config["SECRET_KEY"]
    if not SECRET_KEY:
        raise ValueError("No SECRET_KEY set for Flask application")

    app.secret_key = SECRET_KEY
    app.debug = bool(app.config["DEBUG"])

    app.jinja_env.filters["qs_active"] = qs_active
    app.jinja_env.filters["qs_toggler"] = qs_toggler
    app.jinja_env.trim_blocks = True
    app.jinja_env.lstrip_blocks = True

    app.add_url_rule(
        "/", view_func=Main.as_view("index"), methods=["GET", "POST"]
    )
    app.add_url_rule(
        "<%= snakeName %>", view_func=<%= CamelName %>.as_view(bucket), methods=["GET"]
    )

    # Special view for sending a file
    @app.route("/file/<path:filename>")
    def download_file(filename):
        return flask.send_from_directory(
            os.path.abspath(app.config["<%= UPPERNAME %>_PATH"]), filename
        )

    # Return the app for our factory.
    return app
