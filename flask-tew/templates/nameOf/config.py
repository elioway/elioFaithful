# -*- coding: utf-8 -*-
import os


class BaseConfig(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = "abcdef!£$^&0123456789"
    <%= UPPERNAME %>_USER = "<%= snakeName %>"
    <%= UPPERNAME %>_PASS = "letmein"


class DevelopmentConfig(BaseConfig):
    DEBUG = True
    TESTING = True


class TestingConfig(BaseConfig):
    DEBUG = False
    TESTING = True
