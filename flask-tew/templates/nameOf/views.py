# -*- coding: utf-8 -*-
import flask, flask.views
import functools
import os


def login_required(method):
    @functools.wraps(method)
    def wrapper(*args, **kwargs):
        if "username" in flask.session:
            return method(*args, **kwargs)
        else:
            flask.flash("A login is required to see the page!")
            return flask.redirect(flask.url_for("index"))

    return wrapper


class Main(flask.views.MethodView):
    def get(self):
        return flask.render_template("index.html")

    def post(self):
        USERS = {
            flask.current_app.config["<%= UPPERNAME %>_USER"]: flask.current_app.config[
                "<%= UPPERNAME %>_PASS"
            ]
        }
        if "logout" in flask.request.form:
            flask.session.pop("username", None)
            return flask.redirect(flask.url_for("index"))
        required = ["username", "passwd"]
        for r in required:
            if r not in flask.request.form:
                flask.flash("Error: {0} is required.".format(r))
                return flask.redirect(flask.url_for("index"))
        username = flask.request.form["username"]
        passwd = flask.request.form["passwd"]
        if username in USERS.keys() and USERS[username] == passwd:
            flask.session["username"] = username
        else:
            flask.flash("Username doesn't exist or incorrect password")
        return flask.redirect(flask.url_for("index"))


class <%= CamelName %>(flask.views.MethodView):
    @login_required
    def get(self):
        <%= UPPERNAME %>_USER = flask.current_app.config["<%= UPPERNAME %>_USER"]
        existing_qs_as_dict = flask.request.args.to_dict()
        results = ["e", "l", "i", "o"]
        return flask.render_template(
            "<%= snakeName %>.html",
            results=results,
            user=<%= UPPERNAME %>_USER,
            existing_qs=existing_qs_as_dict,
        )
