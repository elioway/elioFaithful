# -*- coding: utf-8 -*-
from <%= snakeName %>.app import create_app

app = create_app()
