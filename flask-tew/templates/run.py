#!/usr/bin/env python3
#!/usr/bin/env
from <%= snakeName %>.app import create_app

if __name__ == "__main__":
    app = create_app()
    app.run(host="0.0.0.0")
