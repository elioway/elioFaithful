"use strict"
const Thingrator = require("../thingrator")

module.exports = class Apparator extends Thingrator {
  initializing() {
    this.composeWith("thing:repo", {
      arguments: [this.elioName, this.elioGroup],
      composedBy: null,
    })
  }
}
