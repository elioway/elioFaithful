# <%= elioName %> Prerequisites

- [mongoose-bones Prerequisites](https://elioway.gitlab.io/eliobones/mongoose-bones/prerequisites.html)
- [<%= elioGroup %> Prerequisites](../../doc/installing)
- [eliodocked Quickstart](../../elioangels/docked/doc/quickstart)
- [MongoDb docked Quickstart](../../elioangels/docked/doc/quickstart-mongodb-docked)
