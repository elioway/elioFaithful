#!/usr/bin/env node

const <%= camelName %> = require("../<%= kebabName %>")
module.exports = <%= camelName %>
