"use strict"
const Thingrator = require("../thingrator")

module.exports = class Noderator extends Thingrator {
  initializing() {
    if (!this.options.composedBy) {
      this._eliosay("Node the elioWay.")
    }
    this.composeWith("thing:repo", {
      arguments: [this.elioName, this.elioGroup],
      composedBy: "Noderator",
    })
  }
  writing() {
    this.paths()
    let kebabName = this.kebabName
    this._copy("_npmignore", ".npmignore")
    this._copy("_prettierignore", ".prettierignore")
    this._write("nameOf.js", `${kebabName}.js`)
    this._write("package.json")
    this._copy("prettier.config.js")
    this._write("README.md", `README.md`)
    this._write("bin/index.js")
    this._copy("test/always-passes-test.js")
    this._copy("test/freeze-date-test.js")
    this._write("test/nameOf-test.js", `test/${kebabName}-test.js`)
    this._copy("utils/logger.js")
    this._write("doc/installing.md")
    this._write("doc/prerequisites.md")
    this._write("doc/quickstart.md")
  }
  install() {
    if (!this.options.skipinstall) {
      // this.npmInstall()
    }
    this._eliosay("`npm i`")
  }
}
