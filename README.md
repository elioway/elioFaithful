![](https://elioway.gitlab.io/eliofaithful/elio-elioFaithful-logo.png)

> Indoctrinated, **the elioWay**

# elioFaithful ![experimental](/artwork/icon/experimental/favicon.png "experimental")

What you can do when you do things **the elioWay**.

## TODO

- Put generator-thing generators in here.

All of these app form the basis of tutorials.

- [elioFaithful Documentation](https://elioway.gitlab.io/eliofaithful)

## Featured

- [genesis](https://elioway.gitlab.io/eliofaithful/genesis) The elioSin theme example following the [elioSin Quickstart](https://elioway.gitlab.io/eliosin/god/quickstart.html).
- [psalms](https://elioway.gitlab.io/eliofaithful/psalms) A web-based, MP3 player for a personal music collection.

## Nutshell

- [elioFaithful Quickstart](https://elioway.gitlab.io/eliofaithful/quickstart.html)
- [elioFaithful Credits](https://elioway.gitlab.io/eliofaithful/credits.html)

![](https://elioway.gitlab.io/eliofaithful/apple-touch-icon.png)

## License

[MIT](LICENSE) [Tim Bushell](mailto:tcbushell@gmail.com)
