# elioFaithful

<figure>
  <img src="star.png" alt="">
</figure>

> Indoctrinated, **the elioWay**

![experimental](//docs.elioway/./elioangels/icon/experimental/favicon.png "experimental")

## WTF

**elioFaithful** is the place to put any apps or projects which are built **the elioWay**. These are mostly showcase apps, but can be used by other people, not just **elioWay** developers

- [A cult, not a framework](//docs.elioway/./doc/snippet-cult-not-framework)
- [Focused](//docs.elioway/./doc/snippet-focus)

## Faithful

<section>
  <figure>
  <img src="frankenstein/artwork/splash.jpg">
  <h3>It lives</h3>
  <p>A new creature built following <strong>elioThing</strong> quickstarts properly.</p>
  <p><a href="frankenstein"><button><img src="frankenstein/apple-touch-icon.png">frankenstein</button></a></p>
</figure>
  <figure>
  <img src="genesis/artwork/splash.jpg">
  <h3>Let there be egg</h3>
  <p>The product of doing <strong>elioSin</strong> quickstarts properly.</p>
  <p><a href="genesis"><button><img src="genesis/apple-touch-icon.png">genesis</button></a></p>
</figure>
  <figure>
  <img src="psalms/artwork/splash.jpg">
  <h3>Clap your hands, all you nations</h3>
  <p>Web-based MP3 player for your personal music collection.</p>
  <p><a href="psalms"><button><img src="psalms/apple-touch-icon.png">psalms</button></a></p>
</figure>
</section>
