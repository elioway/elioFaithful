# Installing elioFaithful

- [Prerequisites](./prerequisites)

## Contributing

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/eliofaithful.git
cd eliofaithful
git submodule init
git submodule update
```
